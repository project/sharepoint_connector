<?php

namespace Drupal\sharepoint_connector;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\node\NodeInterface;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides methods for interacting with Sharepoint API.
 */
class SharepointConnector implements SharepointConnectorInterface {

  /**
   * The GuzzleHTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  public $client;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The Sharepoint Access Token.
   *
   * @var string
   */
  public $sharepointAccessToken = '';

  /**
   * The Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public $logger;

  /**
   * The FileUrlGenerator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  public $fileUrlGenerator;

  /**
   * Constructs a new Drupal\sharepoint_connector\SharepointConnector object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory object.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The FileUrlGeneratorInterface object.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Logger object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileUrlGeneratorInterface $file_url_generator, LoggerInterface $logger) {
    $this->client = new Client();
    $this->config = $config_factory;
    $this->fileUrlGenerator = $file_url_generator;
    $this->logger = $logger;
    $sharepoint_config = $this->config->get('sharepoint_connector.settings');
    global $config;

    // Load connection information from settings.php $config if it exists
    // Otherwise load from settings form.
    $this->client_id = $config['sharepoint_connector.settings']['client_id'] ?? $sharepoint_config->get('client_id');
    $this->client_secret = $config['sharepoint_connector.settings']['client_secret'] ?? $sharepoint_config->get('client_secret');
    $this->tenant_id = $config['sharepoint_connector.settings']['tenant_id'] ?? $sharepoint_config->get('tenant_id');
    $this->base_url = $config['sharepoint_connector.settings']['host'] ?? $sharepoint_config->get('host');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_url_generator'),
      $container->get('logger.channel.sharepoint_connector')
    );
  }

  /**
   * Prepare fields for sending to Sharepoint.
   *
   * @param array $form_content
   *   Array of content which will be formatted for Sharepoint.
   * @param string $type
   *   Content Type or Webform name to pull settings from.
   *
   * @return array
   *   Formatted array of fields to be sent to Sharepoint
   */
  protected function prepareFields($form_content = [], $type = '') {

    $return_data = [];

    // Look up mapping settings for each field to build array.
    if (is_array($form_content) && isset($form_content['enabled_fields']['map']) && !empty($form_content['enabled_fields']['map'])) {
      $sharepoint_config = $this->config->get('sharepoint_connector.content_types.' . $type);
      $return_data = reset($form_content['enabled_fields']);
      $sharepoint_map_array = $sharepoint_config->get('map', '');
      $sharepoint_map_array = is_array($sharepoint_map_array) && !empty($sharepoint_map_array) ? array_filter($sharepoint_map_array) : '';

      foreach ($return_data as $key => $value) {
        if (isset($sharepoint_map_array[$key]) && !empty($sharepoint_map_array[$key])) {
          $return_data[$sharepoint_map_array[$key]] = $value;
        }
        unset($return_data[$key]);
      }
    }
    return ['fields' => $return_data];
  }

  /**
   * Create a entry in Sharepoint.
   *
   * All fields sent in the form_content should exist in Sharepoint,
   * otherwise you will get an error from the API.
   *
   * We still need to refine how to error handle this.
   *
   * @param array $form_content
   *   Array of content which will be posted to Sharepoint.
   * @param string $url
   *   URL to post to. If empty, will use the base_url.
   * @param string $type
   *   Content Type or Webform name to pull settings from.
   *
   * @return bool
   *   TRUE if successful, FALSE if not.
   *
   * @throws \Exception
   *   Exception thrown when a known error occurs.
   */
  public function postAnEntry($form_content = [], $url = '', $type = '') {

    $sharepoint_config = $this->config->get('sharepoint_connector.settings');
    $is_logging = $sharepoint_config->get('log_requests', '');
    $form_content = $this->prepareFields($form_content, $type);

    if ($this->authenticateToSharepoint()) {
      try {
        $res = $this->client->request(
          'POST',
          (!empty($url) ? $url : $this->base_url),
          [
            'headers' =>
              [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->sharepointAccessToken,
              ],
            'body' =>
            json_encode($form_content),
          ]
        );
      }
      catch (\Exception $e) {
        if ($is_logging) {
          $this->logger->error("ERROR from @file: @line - @message", [
            '@file' => __FILE__,
            '@line' => __LINE__,
            '@message' => $e->getMessage(),
          ]);
        }
      }
      if (isset($res) && $res->getStatusCode() == 201) {
        return TRUE;
      }
      else {
        if ($is_logging) {
          $this->logger->error("ERROR from SharepointConnector class - Request didn't return a 201.");
        }
      }
    }
    else {
      if ($is_logging) {
        $this->logger->error("ERROR from SharepointConnector class - Authentication failed.");
      }
    }
    return FALSE;
  }

  /**
   * Authenticate to Sharepoint.
   *
   * This will set an access token that we need for all subsequent calls.
   *
   * @return bool
   *   TRUE if successful, FALSE if not.
   *
   * @throws \Exception
   *   Exception thrown when a known error occurs.
   */
  protected function authenticateToSharepoint() {
    try {
      $res = $this->client->request(
        'POST',
        'https://login.microsoftonline.com/' . $this->tenant_id . '/oauth2/token?api-version=1.0',
        [
          'form_params' => [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'resource' => 'https://graph.microsoft.com/',
            'grant_type' => 'client_credentials',
          ],
        ]
      );
    }
    catch (\Exception $e) {
      $sharepoint_config = $this->config->get('sharepoint_connector.settings');
      $is_logging = $sharepoint_config->get('log_requests', '');
      if ($is_logging) {
        $this->logger->error("ERROR from @file: @line - @message", [
          '@file' => __FILE__,
          '@line' => __LINE__,
          '@message' => $e->getMessage(),
        ]);
      }
    }

    if (isset($res) && $res->getStatusCode() == 200) {
      $contents = json_decode($res->getBody()->getContents());
      $this->sharepointAccessToken = $contents->access_token;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper function to process node fields.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   */
  public function processNodeFields(NodeInterface $node) {

    $node_type = $node->getType();
    $sharepoint_config = $this->config->get('sharepoint_connector.content_types.' . $node_type);
    $send_array = [];

    $content_type_host = $sharepoint_config->get('url', '');
    $content_type_fields = $sharepoint_config->get('fields', '');
    $content_type_fields = is_array($content_type_fields) ? array_filter($content_type_fields) : '';

    if (!empty($content_type_fields)) {
      $fields = $node->getFields();
      foreach ($fields as $field_name => $field) {
        if (!in_array($node_type . ':' . $field_name, $content_type_fields)) {
          continue;
        }
        $send_array['enabled_fields']['map'][$node_type . ':' . $field_name] = $this->getFieldValue($field);
      }

      if (!empty($send_array)) {
        $this->sendItemToSharepoint($send_array, $content_type_host, $node_type);
      }
    }
  }

  /**
   * Helper function to process webform fields.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   */
  public function processWebformFields(WebformSubmissionInterface $webform_submission) {

    $webform = $webform_submission->getWebform();
    $webform_id = $webform->id();
    $sharepoint_config = $this->config->get('sharepoint_connector.content_types.' . $webform_id);
    $send_array = [];

    $webform_host = $sharepoint_config->get('url', '');
    $enabled_fields = $sharepoint_config->get('fields', '');
    $enabled_fields = is_array($enabled_fields) ? array_filter($enabled_fields) : '';

    if (!empty($enabled_fields)) {
      $webform_fields = $webform->getElementsDecodedAndFlattened();
      $data = $webform_submission->getData();

      foreach ($webform_fields as $field_name => $field_value) {
        if (!empty($field_value) && (isset($field_value['#title']) && !empty($field_value['#title'])) && (isset($field_value['#type']) && $field_value['#type'] != 'processed_text')) {
          if (!in_array($webform_id . ':' . $field_name, array_filter($enabled_fields))) {
            continue;
          }
          $send_array['enabled_fields']['map'][$webform_id . ':' . $field_name] = ($data[$field_name] ?: '');
        }
      }

      if (!empty($send_array)) {
        $this->sendItemToSharepoint($send_array, $webform_host, $webform_id);
      }
    }
  }

  /**
   * Helper function to initiate sending of the formatted array to SharePoint.
   *
   * @param array $send_array
   *   Array of content which will be sent to Sharepoint.
   * @param string $host
   *   URL to post to. If empty, will use the base_url.
   * @param string $send_id
   *   Content Type or Webform name to pull settings from.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response.
   *
   * @throws \Exception
   *   Exception thrown when a known error occurs.
   */
  private function sendItemToSharepoint($send_array, $host, $send_id) {

    $sharepoint_config = $this->config->get('sharepoint_connector.settings');
    $is_logging = $sharepoint_config->get('log_requests', '');

    try {
      $entry = $this->postAnEntry($send_array, $host, $send_id);
    }
    catch (\Exception $e) {
      if ($is_logging) {
        $this->logger->error("ERROR from @file: @line - @message", [
          '@file' => __FILE__,
          '@line' => __LINE__,
          '@message' => $e->getMessage(),
        ]);
      }
    }
    if ($entry) {
      // Message success.
      if ($is_logging) {
        $this->logger->info("SUCCESS! Created new entry in sharepoint with the following info: <pre>@values</pre>", ['@values' => var_export($send_array, TRUE)]);
      }
      $response = new JsonResponse();
      return $response->setStatusCode(JsonResponse::HTTP_CREATED);
    }
    else {
      $response = new JsonResponse();
      if ($is_logging) {
        $this->logger->warning(": <pre>@values</pre>", ['@values' => var_export($send_array, TRUE)]);
      }
      return $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
    }
  }

  /**
   * Helper function to get field values by field type.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $field
   *   The field object.
   *
   * @return array|mixed|string
   *   The field value.
   */
  private function getFieldValue($field) {
    $field_type = $field->getFieldDefinition()->getType();
    switch ($field_type) {
      case 'list_string':
      case 'string':
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        // Text fields (includes plain, formatted, long, etc.)
        return $field->value;

      case 'entity_reference':
        // Entity reference fields (e.g., taxonomy term reference)
        // $target_ids = $field->getValue();
        // This gives you an array of entity IDs.
        // You might want to load the entities too:
        $entities = $field->referencedEntities();
        if (!empty($entities)) {
          $values = [];
          foreach ($entities as $entity) {
            $values[] = $entity->label();
          }
          $values = implode(', ', $values);
          return $values;
        }
        break;

      case 'image':
      case 'file':
        // Image and file fields.
        // $target_ids = $field->getValue();
        // This gives you an array of file entity IDs.
        $files = $field->referencedEntities();
        if (!empty($files)) {
          $uri = $files[0]->getFileUri();
          $url = $this->fileUrlGenerator->generateAbsoluteString($uri);
          if (!empty($url)) {
            return $url;
          }
        }
        break;

      case 'datetime':
        // Date fields.
        return $field->date;

      case 'boolean':
      case 'integer':
      case 'decimal':
      case 'float':
        return (!empty($field->value) ? $field->value : 0);

      // ... Handle other field types as needed
      default:
        // Some custom field types might not fall into the above categories.
        $values = $field->getValue();
        return $values;
    }
  }

}
