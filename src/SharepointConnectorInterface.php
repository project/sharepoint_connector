<?php

namespace Drupal\sharepoint_connector;

use Drupal\node\NodeInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Sharepoint Connector service interface.
 */
interface SharepointConnectorInterface {

  /**
   * Create a entry in Sharepoint.
   *
   * @param array $form_content
   *   Array of content which will be posted to Sharepoint.
   * @param string $url
   *   URL to post to. If empty, will use the base_url.
   * @param string $type
   *   Content Type or Webform name to pull settings from.
   *
   * @return bool
   *   TRUE if successful, FALSE if not.
   *
   * @throws \Exception
   *   Exception thrown when a known error occurs.
   */
  public function postAnEntry($form_content = [], $url = '', $type = '');

  /**
   * Helper function to process node fields.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   */
  public function processNodeFields(NodeInterface $node);

  /**
   * Helper function to process webform fields.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   */
  public function processWebformFields(WebformSubmissionInterface $webform_submission);

}
