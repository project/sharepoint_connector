<?php

namespace Drupal\sharepoint_connector\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form listing page for the Sharepoint Connector - Content Types.
 */
class SharepointConnectorFormContentTypes extends FormBase {

  const FORM_ID = 'sharepoint_connector_settings_types_form';

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Drupal\sharepoint_connector\Form\SharepointConnectorFormContentTypes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sharepoint_connector.content_types'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $content_types = $this->getContentTypes();

    $form['typetable'] = [
      '#type' => 'table',
      '#header' => [$this->t('Content Type'), $this->t('Operations')],
      '#empty' => $this->t('There are no items yet.'),
    ];

    foreach ($content_types as $type => $type_label) {

      // Skip webform type since it will be handled in its own form.
      if ($type == 'webform') {
        continue;
      }

      $form['typetable'][$type]['type_label'] = [
        '#plain_text' => $type_label,
      ];

      // Operations (dropbutton) column.
      $form['typetable'][$type]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];

      $sharepoint_config = $this->config('sharepoint_connector.content_types.' . $type);
      $content_type_host = $sharepoint_config->get('url', '');
      $content_type_fields = $sharepoint_config->get('fields', '');

      if (!empty($content_type_host) || (is_array($content_type_fields) && !empty(array_filter($content_type_fields)))) {
        $form['typetable'][$type]['operations']['#links']['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('sharepoint_connector.manage_edit',
            ['id' => $type_label, 'machine' => $type]),
        ];
      }
      else {
        $form['typetable'][$type]['operations']['#links']['add'] = [
          'title' => $this->t('Add'),
          'url' => Url::fromRoute('sharepoint_connector.manage_edit',
            ['id' => $type_label, 'machine' => $type]),
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

  /**
   * Helper function to get a list of content types.
   *
   * @return array
   *   An array of content type labels keyed by their machine names.
   */
  protected function getContentTypes() {
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($content_types as $content_type) {
      $options[$content_type->id()] = $content_type->label();
    }
    return $options;
  }

}
