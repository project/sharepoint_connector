<?php

namespace Drupal\sharepoint_connector\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form listing page for the Sharepoint Connector - Webforms.
 */
class SharepointConnectorFormWebforms extends FormBase {

  const FORM_ID = 'sharepoint_connector_settings_webforms_form';

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Drupal\sharepoint_connector\Form\SharepointConnectorFormWebforms object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sharepoint_connector.content_types'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $webforms = $this->getWebforms();

    $form['typetable'] = [
      '#type' => 'table',
      '#header' => [$this->t('Webform'), $this->t('Operations')],
      '#empty' => $this->t('There are no items yet.'),
    ];

    foreach ($webforms as $webform => $webform_label) {

      $form['typetable'][$webform]['type_label'] = [
        '#plain_text' => $webform_label,
      ];

      // Operations (dropbutton) column.
      $form['typetable'][$webform]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];

      $sharepoint_config = $this->config('sharepoint_connector.content_types.' . $webform);
      $webform_host = $sharepoint_config->get('url', '');
      $webform_fields = $sharepoint_config->get('fields', '');

      if (!empty($webform_host) || (is_array($webform_fields) && !empty(array_filter($webform_fields)))) {
        $form['typetable'][$webform]['operations']['#links']['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('sharepoint_connector.manage_webforms_edit',
            ['id' => $webform_label, 'machine' => $webform]),
        ];
      }
      else {
        $form['typetable'][$webform]['operations']['#links']['add'] = [
          'title' => $this->t('Add'),
          'url' => Url::fromRoute('sharepoint_connector.manage_webforms_edit',
            ['id' => $webform_label, 'machine' => $webform]),
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

  /**
   * Helper function to get a list of webforms.
   *
   * @return array
   *   An array of webform labels keyed by their machine names.
   */
  protected function getWebforms() {
    $webforms = $this->entityTypeManager->getStorage('webform')->loadMultiple();
    $options = [];
    foreach ($webforms as $webform) {
      $options[$webform->id()] = $webform->label();
    }
    return $options;
  }

}
