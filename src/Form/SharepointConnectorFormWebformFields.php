<?php

namespace Drupal\sharepoint_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configuration form for the Sharepoint Connector - Webform Fields.
 */
class SharepointConnectorFormWebformFields extends ConfigFormBase {

  const FORM_ID = 'sharepoint_connector_settings_form_webform_fields';

  /**
   * The Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new Drupal\sharepoint_connector\Form\SharepointConnectorFormWebformFields object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Request Stack object.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $webform_id = $this->requestStack->getCurrentRequest()->query->get('machine');
    return ['sharepoint_connector.content_types.' . $webform_id];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $webform_label = $this->requestStack->getCurrentRequest()->query->get('id');
    $webform_id = $this->requestStack->getCurrentRequest()->query->get('machine');
    $sharepoint_config = $this->config('sharepoint_connector.content_types.' . $webform_id);

    // Create a field group.
    $form['host'] = [
      '#type' => 'details',
      '#title' => $this->t('Connection'),
      '#open' => TRUE,
    ];

    $form['host']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform @label Sharepoint Host URL', ['@label' => $webform_label]),
      '#description' => $this->t('Enter the host url of your Sharepoint list (leave blank for default)'),
      '#default_value' => $sharepoint_config->get('url'),
      '#maxlength' => 255,
    ];

    $form['enabled_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Enabled Fields'),
      '#open' => TRUE,
    ];

    // Get all fields for this webform.
    $webform_fields = $this->getWebformFields($webform_id);
    $fields_default = $sharepoint_config->get('fields') ?? [];

    $form['enabled_fields']['fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('@label fields', ['@label' => $webform_label]),
      '#options' => $webform_fields,
      '#default_value' => $fields_default,
    ];

    $map_default = $sharepoint_config->get('map', '');
    foreach ($webform_fields as $field_key => $field_label) {
      $field_default = (is_array($map_default) && isset($map_default[$field_key]) && !empty($map_default[$field_key])) ? $map_default[$field_key] : '';
      $form['enabled_fields']['map'][$field_key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Sharepoint field mapping for Drupal field (@label)', ['@label' => $field_label]),
        '#description' => $this->t('Enter the Sharepoint field that this Drupal field should map to'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $field_key . ']"]' => ['checked' => TRUE],
          ],
        ],
        '#default_value' => $field_default,
        '#maxlength' => 255,
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $webform_id = $this->requestStack->getCurrentRequest()->query->get('machine');

    $this->config('sharepoint_connector.content_types.' . $webform_id)
      ->set('url', $form_state->getValue('url'))
      ->set('fields', $form_state->getValue('fields'))
      ->save();

    $mapped_fields = $form_state->getValues();
    if (!empty($mapped_fields)) {
      $map = [];
      foreach ($mapped_fields as $field_key => $field_mapping) {
        if (!in_array($field_key, ['submit', 'form_build_id', 'form_token', 'form_id', 'op', 'fields', 'url'])) {
          $map[$field_key] = $field_mapping;
        }
      }
      $this->config('sharepoint_connector.content_types.' . $webform_id)
        ->set('map', $map)
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to get a list of fields for webforms.
   *
   * @return array
   *   An array of field labels keyed by their machine names.
   */
  protected function getWebformFields($webform_key) {
    $webform = Webform::load($webform_key);
    $webform_fields = $webform->getElementsDecodedAndFlattened();
    $fields = [];

    foreach ($webform_fields as $field_name => $field_definition) {
      if (!empty($field_definition) && (isset($field_definition['#title']) && !empty($field_definition['#title'])) && (isset($field_definition['#type']) && $field_definition['#type'] != 'processed_text')) {
        $fields[$webform_key . ':' . $field_name] = $field_definition['#title'];
      }
    }
    $fields[$webform_key . ':created'] = 'Created';
    return $fields;
  }

}
