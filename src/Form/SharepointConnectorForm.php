<?php

namespace Drupal\sharepoint_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the Sharepoint Connector.
 */
class SharepointConnectorForm extends ConfigFormBase {

  const FORM_ID = 'sharepoint_connector_settings_form';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sharepoint_connector.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $sharepoint_config = $this->config('sharepoint_connector.settings');
    global $config;

    // Create a field group.
    $form['connection_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Connection'),
      '#open' => TRUE,
    ];

    // Connection Type.
    $form['connection_fieldset']['connection_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Connection Type'),
      '#options' => [
        'client_tenant_id' => $this->t('Client/Tenant ID'),
      ],
      '#description' => $this->t('Select the type of connection you would like to use to connect to Sharepoint.'),
      '#default_value' => $sharepoint_config->get('connection_type'),
    ];

    $form['connection_fieldset']['disable_api'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable all calls to Sharepoint'),
      '#description' => $this->t('Killswitch - disable Sharepoint API connections entirely.'),
      '#default_value' => $sharepoint_config->get('disable_api'),
      '#prefix' => '<span style="line-height: 5px;">&nbsp;</span>',
    ];

    $form['connection_fieldset']['log_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log requests'),
      '#description' => $this->t('Log all Sharepoint requests and responses.'),
      '#default_value' => $sharepoint_config->get('log_requests'),
    ];

    // Create a field group.
    $form['keys_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Keys'),
      '#open' => TRUE,
    ];

    // Client ID.
    $form['keys_fieldset']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Enter your Microsoft Graph API Client ID.'),
      '#default_value' => $sharepoint_config->get('client_id'),
    ];

    if (isset($config['sharepoint_connector.settings']['client_id'])) {
      $form['keys_fieldset']['client_id']['#default_value'] = '';
      $form['keys_fieldset']['client_id']['#disabled'] = TRUE;
      $form['keys_fieldset']['client_id']['#description'] = $this->t('@orig_desc (Overriden by settings.php)', ['@orig_desc' => $form['keys_fieldset']['client_id']['#description']]);
    }

    // Client Secret.
    $form['keys_fieldset']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('Enter your Microsoft Graph API Client Secret (Insecure Method)'),
      '#default_value' => $sharepoint_config->get('client_secret'),
    ];

    if (isset($config['sharepoint_connector.settings']['client_secret'])) {
      $form['keys_fieldset']['client_secret']['#default_value'] = '';
      $form['keys_fieldset']['client_secret']['#disabled'] = TRUE;
      $form['keys_fieldset']['client_secret']['#description'] = $this->t('@orig_desc (Overriden by settings.php)', ['@orig_desc' => $form['keys_fieldset']['client_secret']['#description']]);
    }

    // Tenant ID.
    $form['keys_fieldset']['tenant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tenant ID'),
      '#description' => $this->t('Enter your Microsoft Graph API Tenant ID.'),
      '#default_value' => $sharepoint_config->get('tenant_id'),
    ];

    if (isset($config['sharepoint_connector.settings']['tenant_id'])) {
      $form['keys_fieldset']['tenant_id']['#default_value'] = '';
      $form['keys_fieldset']['tenant_id']['#disabled'] = TRUE;
      $form['keys_fieldset']['tenant_id']['#description'] = $this->t('@orig_desc (Overriden by settings.php)', ['@orig_desc' => $form['keys_fieldset']['tenant_id']['#description']]);
    }

    // Create a field group.
    $form['host_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Host'),
      '#open' => TRUE,
    ];

    // Host / Base URL.
    $form['host_fieldset']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL'),
      '#description' => $this->t('Enter the host url of your Sharepoint list.'),
      '#default_value' => $sharepoint_config->get('host'),
      '#maxlength' => 255,
    ];

    if (isset($config['sharepoint_connector.settings']['host'])) {
      $form['host_fieldset']['host']['#default_value'] = '';
      $form['host_fieldset']['host']['#disabled'] = TRUE;
      $form['host_fieldset']['host']['#description'] = $this->t('@orig_desc (Overriden by settings.php)', ['@orig_desc' => $form['host_fieldset']['host']['#description']]);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sharepoint_connector.settings')
      ->set('connection_type', $form_state->getValue('connection_type'))
      ->set('disable_api', $form_state->getValue('disable_api'))
      ->set('log_requests', $form_state->getValue('log_requests'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('tenant_id', $form_state->getValue('tenant_id'))
      ->set('host', $form_state->getValue('host'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
