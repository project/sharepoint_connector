<?php

namespace Drupal\sharepoint_connector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sharepoint_connector\SharepointConnector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides controller methods for the SharePoint Connector.
 */
class SharepointController extends ControllerBase {

  /**
   * The Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Sharepoint Connector.
   *
   * @var \Drupal\sharepoint_connector\SharepointConnector
   */
  protected $sharepointConnector;

  /**
   * Constructs a new Drupal\sharepoint_connector\Controller\SharepointController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Request Stack object.
   * @param \Drupal\sharepoint_connector\SharepointConnector $sharepoint_connector
   *   The Sharepoint Connector object.
   */
  public function __construct(RequestStack $request_stack, SharepointConnector $sharepoint_connector) {
    $this->requestStack = $request_stack;
    $this->sharepointConnector = $sharepoint_connector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('sharepoint_connector.connector')
    );
  }

  /**
   * Posts an entry to SharePoint.
   *
   * The payload for the post operation is derived from the current request.
   * This method will log appropriate messages based on the result of the
   * post operation - whether it succeeds or fails.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A response with a status code of either 201 (Created) if the post
   *   succeeds or 404 (Not Found) if it fails.
   */
  public function postEntry() {
    $payload = $this->buildPayload();

    if (!$payload) {
      $response = new JsonResponse();
      return $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
    }

    $sharepoint_config = $this->config('sharepoint_connector.settings');
    $is_logging = $sharepoint_config->get('connection_fieldset', '');
    $is_logging = (isset($is_logging['log_requests']) && !empty($is_logging['log_requests'])) ? $is_logging['log_requests'] : '';

    try {
      $entry = $this->sharepointConnector->postAnEntry($payload);

      if ($entry) {
        if ($is_logging) {
          $this->sharepointConnector->logger->info("Entry successfully posted to SharePoint.");
        }
        $response = new JsonResponse();
        return $response->setStatusCode(JsonResponse::HTTP_CREATED);
      }
      else {
        if ($is_logging) {
          $this->sharepointConnector->logger->warning("Failed to post entry to SharePoint.");
        }
        $response = new JsonResponse();
        return $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
      }
    }
    catch (\Exception $e) {
      if ($is_logging) {
        $this->sharepointConnector->logger->error("An error occurred while posting to SharePoint: @error", ['@error' => $e->getMessage()]);
      }
    }
  }

  /**
   * Constructs the payload for the SharePoint post operation.
   *
   * Extracts data from the current request and performs any necessary
   * filtering or transformations, like removing certain fields.
   *
   * @return array
   *   An associative array containing the payload data.
   */
  public function buildPayload() {
    $payload = $this->requestStack->getCurrentRequest()->request->all();

    // Filter out the 'metatag' field if it exists.
    if (isset($payload['metatag'])) {
      unset($payload['metatag']);
    }

    return $payload;
  }

}
