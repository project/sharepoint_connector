<?php

/**
 * @file
 * Sharepoint Connector module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Implements hook_help().
 */
function sharepoint_connector_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    // Main module help for the sharepoint_connector module.
    case 'help.page.sharepoint_connector':
      return '<p>' . t('This module provides the ability to sync content being saved to individual content types or webform submissions with individual SharePoint lists. The <a href=":settings">SharePoint Connector Configuration page</a> provides a form for setting the basic module settings.', [
        ':settings' => Url::fromRoute('sharepoint_connector.settings')
          ->toString(),
      ]) . '</p>';

    // Help for the main settings page.
    case 'sharepoint_connector.settings':
      return '<p>' . t('This page provides a form for setting the basic authentication details and base host url if they are not already defined in the settings.php file as well as additional module options (i.e. disable all API calls and log all requests). Remember that your changes will not be saved until you click the <em>Save configuration</em> button at the bottom of the page.') . '</p>';

    // Help for the content types page.
    case 'sharepoint_connector.content_types':
      return '<p>' . t('This page provides a list of all content types on the site. Individual content types can be enabled to allow sending of data to SharePoint.') . '</p>';

    // Help for the webforms page.
    case 'sharepoint_connector.webforms':
      return '<p>' . t('This page provides a list of all webforms on the site. Individual webforms can be enabled to allow sending of webform submission data to SharePoint.') . '</p>';
  }
}

/**
 * Implements hook_node_insert().
 */
function sharepoint_connector_node_insert(NodeInterface $node) {

  if (!_sharepoint_connector_get_disabled_api()) {
    $sharepoint = \Drupal::service('sharepoint_connector.connector');
    $sharepoint->processNodeFields($node);
  }
}

/**
 * Implements hook_node_update().
 */
function sharepoint_connector_node_update(NodeInterface $node) {

  if (!_sharepoint_connector_get_disabled_api()) {
    $sharepoint = \Drupal::service('sharepoint_connector.connector');
    $sharepoint->processNodeFields($node);
  }
}

/**
 * Implements hook_webform_submission_insert().
 */
function sharepoint_connector_webform_submission_insert(WebformSubmissionInterface $webform_submission) {

  if (!_sharepoint_connector_get_disabled_api()) {
    $sharepoint = \Drupal::service('sharepoint_connector.connector');
    $sharepoint->processWebformFields($webform_submission);
  }
}

/**
 * Helper function to check if API is disabled.
 */
function _sharepoint_connector_get_disabled_api() {
  $sharepoint_config = \Drupal::config('sharepoint_connector.settings');
  $api_disabled = $sharepoint_config->get('disable_api', 0);
  return $api_disabled;
}
